import static ui.UiMenu.showMenu;

public class Main {
    public static void main(String[] args) {

        showMenu();

        Doctor myDoctor = new Doctor("Tyroone", "Internista");
        System.out.println(myDoctor.name);
        System.out.println(myDoctor.speciality);

        Patient patient = new Patient("Alejandra", "Alejandra@bich.com");
        System.out.println(patient.name);
        System.out.println(patient.email);

        patient.weight = 60.5;
        patient.heigh = 1.72;

        System.out.println(patient.weight);
        System.out.println(patient.heigh);
    }



}